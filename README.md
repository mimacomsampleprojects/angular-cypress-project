# AngularCypressApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.


# Instructions for setting up testcube on local minicube cluster

## requirements
- Docker Desktop installed
- Angular Application
- Cypress Tests in Angular written
- minicube cluster installed and run on your local maschine
- HELM 

## install Testcube

[testcube Installation Link](https://testkube.io/)

1. brew Installing Testkube CLI
    ```shell
    brew install testkube
    ``` 
2. make sure minikube is running
    ```shell
    minikube status
    ``` 
3. install testkube in minikube cluster
     ```shell
    testkube init
    ``` 
    - in the installation process you have to select the 

## Configure and run tests with Testcube

1. start Dashboard
    ```shell
    testkube Dashboard
    ``` 
2. in dem neuen Browserfenster können nun tests erstellt werden.
    - dazu muss Repository in dem sich Quellcode und Tests befinden
    hinterlegt werden und Credentials für zugriff auf Repository erstellt werden

3. Die Anwendung muss das Docker Image gebaut werden und in registry gepushed werden

4. Die Anwendung muss in Kubernetes deployed werden
    ```shell
    kubectl apply -f angular-deployment.yaml
    ``` 
5. In *cypress.config.ts* muss URL für für Service angepasst werden
    ```shell
    e2e: {
    'baseUrl': 'http://angular-service.angular-website'
    },
    
    # URL Syntax
    SYNTAX:  http://[SERVICENAME].[NAMESPACE-NAME] 
    ```
    

## test code changes

1. Push new Created Tests into Remote Code Repository
2. Create container Image with new code changes
    ```shell
    docker build -t registry.gitlab.com/mimacomsampleprojects/angular-cypress-project:v.0.0.2 . 
    ``` 
3. Upload Image to Registry
    ```shell
    docker push registry.gitlab.com/mimacomsampleprojects/angular-cypress-project:v0.0.2
    ``` 
4. deploy Image to Kubernetes
    ```shell
    kubectl apply -f DEPLOYMENT-FILE.yaml
    ``` 
5. Run Tests and see results


