
# stage 1
FROM node:latest AS AngularBuildImage
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build


# stage 2
FROM nginx:alpine
COPY --from=AngularBuildImage /app/dist/angular-cypress-app /usr/share/nginx/html