describe('My First Test', () => {
  it('Visits the initial project page', () => {
    cy.visit('/')
    cy.contains('test Page')
  })



  it('can you add two numbers',() =>   {
    cy.visit('/')
    
    cy.get('[data-cy="addNumbersButton"]').click()

    cy.get('[data-cy="calculationResult"]').should('have.value', '5')
    
  })
})
